<?php

include('template_modules.inc');

/*
 * Give a count of content per type
 */

function count_nodes_per_content_type() {
    $node_counts = array();
    $result = db_query('SELECT n.type, COUNT(n.type) FROM {node} n GROUP BY n.type ORDER BY COUNT(n.type) DESC')->fetchAllKeyed();
    foreach ($result as $type => $count) {
      $node_counts[] = "$type => $count";
  }
    hacky_print('Nodes per Content Type', $node_counts);
}

/*
 * Give a count of users per role
 */

function count_users_per_role() {
    $role_counts = array();
    $result = db_query('SELECT r.name, COUNT(ur.rid) FROM {role} r JOIN users_roles ur ON r.rid=ur.rid GROUP BY ur.rid ORDER BY COUNT(ur.rid) DESC')->fetchAllKeyed();
    foreach ($result as $role => $count) {
      $role_counts[] = "$role => $count";
  }
    hacky_print('Users per Role', $role_counts);
}

/*
 * find all fields that have php input format and generate a report
 */

function input_check ($input = 'php') {
  $table_list = db_query("show tables")->fetchAllKeyed();
  $contains_format = array();
  foreach ($table_list as $tablename => $placeholder) {
    $check_format = db_query("show columns from $tablename like '%format%'");
    foreach ($check_format->fetchAll() as $field) {
      $field_count = db_query("select count(*) as count from {$tablename} where $field->Field like '%$input%'")->fetchCol();
      if ($field_count[0] > 0) {
        $contains_format[] = "table $tablename appears to have $field_count[0] fields containing $input in field $field->Field";
      }
    }

  }

  hacky_print('Input Check', $contains_format);
}

/*
 * Count the number of enabled modules
 */

function module_count() {
  $result = db_query("SELECT COUNT(s.name) AS count FROM {system} s WHERE s.status = 1")->fetchCol();
  hacky_print('Module Count', $result);
}

/*
 * Check modules available in the template vs. modules available in site
 */

function check_modules() {
  global $template_modules;
  $site_modules = array_keys(system_rebuild_module_data());
  $comparison1 = array_diff($site_modules, $template_modules);
  $comparison2 = array_diff($template_modules, $site_modules);
  
  if (isset($comparison1) || isset($comparison2)) {
    hacky_print('Modules only found in Site', $comparison1);
    hacky_print('Modules removed from template', $comparison2);
  } else {
    hacky_print('Check_modules', array('No differences found.'));
  }
}

/*
 *
 */

function check_superuser() {
  $result = db_query("SELECT uid, pass from {users} where uid = 1")->fetchAllKeyed();
  $retval = (isset($result[1]) && $result[1] == '$S$DfYN/INCGpaoYHVYmcOR91HA2un5750Nj1RFnv/ZIbcJ9RGUhIiI') ? 'TRUE' : 'FALSE';
  hacky_print('Check Superuser', array($retval));
}

function check_system_cache() {
  //$results = system_cacheaudit();
  $all_good = TRUE;
  

  $return_arr = array();
  $variable_check = array(
  	'block_cache' => 'Block Cache',
  	'cache' => 'Page Cache',
  	'preprocess_css' => 'Pre-Process CSS',
  	'preprocess_js' => 'Pre-Process JS', 
  	);
  foreach ($variable_check as $var => $readable) {
  	$value = variable_get($var, 'NOT FOUND');
    $return_arr[] = array($readable => $value);
  }
  hacky_print('Check System Cache', $return_arr);
}

function hacky_print($title, $results) {
  print "title: " . $title . "\n";
  foreach($results as $result) {
  	if (is_array($result)) {
  	  print_r($result);
  	} else {
  	  print "  " . $result . "\n";
  	}
  }
  print "\n\n####\n\n";

} 

count_nodes_per_content_type();
count_users_per_role();
//input_check();
//input_check('html');
module_count();
check_superuser();
check_modules();
check_system_cache();
